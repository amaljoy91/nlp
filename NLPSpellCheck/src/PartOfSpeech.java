import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;



public class PartOfSpeech {

static Map<String,Triwords> trigrams=new HashMap<String,Triwords>();
static Map<String,Integer> wordCount=new HashMap<String,Integer>();
public Map<String,Triwords> getAllfiles() throws Exception
{
File folder = new File("brown");
File[] listOfFiles = folder.listFiles();

for (File file : listOfFiles) {
    if (file.isFile() && file.getName().length()==4) {
    	//System.out.println(file.getName());
    	trigrams= readTrigrams(file.getName());
    	
    }
}

Iterator iterator = wordCount.keySet().iterator();  

while (iterator.hasNext()) {  
    String key = (String) iterator.next();
    int count =wordCount.get(key);
  //  if(count<5)
//System.out.println("key "+key+"count"+count);
}
return trigrams;
}



public Map<String,Triwords> readTrigrams(String filename) throws Exception{
	BufferedReader br = new BufferedReader(new FileReader("brown/"+filename));
    try {
        StringBuilder sb = new StringBuilder();
        String line = br.readLine();

        while (line != null) {
            sb.append(line);
            sb.append(" ");
            line = br.readLine();
        }
        String everything = sb.toString().trim().toLowerCase();
        String[] words1 = everything.split("\\s+");
        ArrayList<String> words = new ArrayList<String>(Arrays.asList(words1));
        words.removeAll(Collections.singleton("the/at"));
        words.removeAll(Collections.singleton("the/at-tl"));
        words.removeAll(Collections.singleton("of/in"));
        words.removeAll(Collections.singleton("and/cc"));
        words.removeAll(Collections.singleton("a/at"));
        words.removeAll(Collections.singleton(",/,"));
        words.removeAll(Collections.singleton("--/--"));
        words.removeAll(Collections.singleton("./."));
        words.removeAll(Collections.singleton("and/cc"));
        words.removeAll(Collections.singleton("to/to"));
        words.removeAll(Collections.singleton("in/in"));
        words.removeAll(Collections.singleton("to/in"));
        words.removeAll(Collections.singleton("in/rp"));
        words.removeAll(Collections.singleton("in/in"));
        words.removeAll(Collections.singleton("is/bez"));
        words.removeAll(Collections.singleton("it/pps"));
        words.removeAll(Collections.singleton("for/in"));
        words.removeAll(Collections.singleton("was/bedz"));
        words.removeAll(Collections.singleton("it/pps"));
        words.removeAll(Collections.singleton("that/cs"));
        words.removeAll(Collections.singleton("he/pps"));
        words.removeAll(Collections.singleton("''/''"));
        words.removeAll(Collections.singleton("``/``"));
        words.removeAll(Collections.singleton("as/cs"));
        words.removeAll(Collections.singleton("as/ql"));
        words.removeAll(Collections.singleton("his/pp$"));
        words.removeAll(Collections.singleton("with/in"));
        words.removeAll(Collections.singleton("by/in"));
        words.removeAll(Collections.singleton("had/hvd"));
        words.removeAll(Collections.singleton("on/in"));
        words.removeAll(Collections.singleton("this/dt"));
        words.removeAll(Collections.singleton("be/be"));
        words.removeAll(Collections.singleton(";/."));
        words.removeAll(Collections.singleton("i/ppss"));
        words.removeAll(Collections.singleton("on/rp"));
        words.removeAll(Collections.singleton("at/in"));
        words.removeAll(Collections.singleton("into/in"));
        words.removeAll(Collections.singleton(":/:"));
        words.removeAll(Collections.singleton("all/abn"));
        words.removeAll(Collections.singleton("you/ppss"));
        words.removeAll(Collections.singleton("which/wdt"));
        words.removeAll(Collections.singleton("then/rb"));
        words.removeAll(Collections.singleton("an/at"));
        words.removeAll(Collections.singleton("they/ppss"));
        words.removeAll(Collections.singleton("but/cc"));
        words.removeAll(Collections.singleton("more/ap"));
        words.removeAll(Collections.singleton("may/md"));
        words.removeAll(Collections.singleton("its/pp$"));
        words.removeAll(Collections.singleton("not/*"));
        words.removeAll(Collections.singleton("only/rb"));
        words.removeAll(Collections.singleton("will/md"));
        words.removeAll(Collections.singleton("him/ppo"));
        words.removeAll(Collections.singleton("any/dti"));
        words.removeAll(Collections.singleton("are/ber"));
        words.removeAll(Collections.singleton("more/ql"));
        words.removeAll(Collections.singleton("if/cs"));
        words.removeAll(Collections.singleton("have/hv"));
        words.removeAll(Collections.singleton("so/ql"));
        words.removeAll(Collections.singleton("there/ex"));
        words.removeAll(Collections.singleton("which/wdt"));
        words.removeAll(Collections.singleton("or/cc"));
        words.removeAll(Collections.singleton("(/("));
        words.removeAll(Collections.singleton(")/)"));
        words.removeAll(Collections.singleton("out/in"));
        words.removeAll(Collections.singleton("these/dts"));
        words.removeAll(Collections.singleton("about/rb"));
        words.removeAll(Collections.singleton("her/pp$"));
        words.removeAll(Collections.singleton("were/bed"));
        words.removeAll(Collections.singleton("up/rp"));
        words.removeAll(Collections.singleton("?/."));
        words.removeAll(Collections.singleton("would/md"));
        words.removeAll(Collections.singleton("has/hvz"));
        words.removeAll(Collections.singleton("from/in"));
        words.removeAll(Collections.singleton("that/dt"));
        words.removeAll(Collections.singleton("from/in-tl"));
        words.removeAll(Collections.singleton("their/pp$"));
        words.removeAll(Collections.singleton("we/ppss"));
        words.removeAll(Collections.singleton("she/pps"));
        words.removeAll(Collections.singleton("it/ppo"));
        words.removeAll(Collections.singleton("when/wrb"));
        words.removeAll(Collections.singleton("who/wps"));
        words.removeAll(Collections.singleton("been/ben"));
        words.removeAll(Collections.singleton("no/ql"));
        words.removeAll(Collections.singleton("one/cd"));
        words.removeAll(Collections.singleton("no/at"));
        words.removeAll(Collections.singleton("no/rb-nc"));
        words.removeAll(Collections.singleton("it/pps-hl"));
        words.removeAll(Collections.singleton("she/pps-nc"));
        words.removeAll(Collections.singleton("do/do"));
        words.removeAll(Collections.singleton("out/rp"));
        words.removeAll(Collections.singleton("that/wps"));
        words.removeAll(Collections.singleton("of/in-tl"));
        words.removeAll(Collections.singleton("also/rb"));
        words.removeAll(Collections.singleton("our/pp$"));
        words.removeAll(Collections.singleton("than/cs"));
        words.removeAll(Collections.singleton("such/jj"));
        words.removeAll(Collections.singleton("like/cs"));
        words.removeAll(Collections.singleton("what/wdt"));
        words.removeAll(Collections.singleton("could/md"));
        words.removeAll(Collections.singleton("some/dti"));
        words.removeAll(Collections.singleton("them/ppo"));
        words.removeAll(Collections.singleton("!/."));
        words.removeAll(Collections.singleton("could/md"));
        words.removeAll(Collections.singleton("said/vbd"));
        words.removeAll(Collections.singleton("new/jj"));
        words.removeAll(Collections.singleton("new/jj-tl"));
        words.removeAll(Collections.singleton("can/md"));
        words.removeAll(Collections.singleton("other/ap"));
        words.removeAll(Collections.singleton("time/nn"));
        words.removeAll(Collections.singleton("time/nn-tl"));
        words.removeAll(Collections.singleton("about/in"));
        words.removeAll(Collections.singleton("my/pp$"));
        words.removeAll(Collections.singleton("must/md"));
        words.removeAll(Collections.singleton("after/in"));
        words.removeAll(Collections.singleton("over/in"));
        words.removeAll(Collections.singleton("me/ppo"));
        words.removeAll(Collections.singleton("man/nn"));
        words.removeAll(Collections.singleton("over/rp"));
        words.removeAll(Collections.singleton("did/dod"));
        words.removeAll(Collections.singleton("before/cs"));
        words.removeAll(Collections.singleton("after/cs"));
        words.removeAll(Collections.singleton("first/rb"));
        words.removeAll(Collections.singleton("made/vbd"));
        words.removeAll(Collections.singleton("so/rb"));
        words.removeAll(Collections.singleton("many/ap"));
        words.removeAll(Collections.singleton("most/ql"));
        words.removeAll(Collections.singleton("even/rb"));
        words.removeAll(Collections.singleton("her/ppo"));
        words.removeAll(Collections.singleton("first/od"));
        words.removeAll(Collections.singleton("now/rb"));
        words.removeAll(Collections.singleton("two/cd"));
        words.removeAll(Collections.singleton("two/cd-hl"));
        words.removeAll(Collections.singleton("those/dts"));
        words.removeAll(Collections.singleton("where/wrb"));
        words.removeAll(Collections.singleton("people/nns"));
        words.removeAll(Collections.singleton("just/rb"));
        words.removeAll(Collections.singleton("your/pp$"));
        words.removeAll(Collections.singleton("good/jj"));
        words.removeAll(Collections.singleton("how/wrb"));
        words.removeAll(Collections.singleton("just/rb"));
        words.removeAll(Collections.singleton("you/ppo"));
        words.removeAll(Collections.singleton("mr./np"));
        words.removeAll(Collections.singleton("back/rb"));
        words.removeAll(Collections.singleton("through/in"));
        words.removeAll(Collections.singleton("down/rp"));
        words.removeAll(Collections.singleton("should/md"));
        words.removeAll(Collections.singleton("way/nn"));
        words.removeAll(Collections.singleton("because/cs"));
        words.removeAll(Collections.singleton("much/ap"));
        words.removeAll(Collections.singleton("well/rb"));
        words.removeAll(Collections.singleton("little/ap"));
        words.removeAll(Collections.singleton("little/ql"));
        words.removeAll(Collections.singleton("each/dt"));
        words.removeAll(Collections.singleton("years/nns"));
        words.removeAll(Collections.singleton("af/nn"));
        words.removeAll(Collections.singleton("too/ql"));
        
        
        int i;
       
        
     //   if(words.contains("moon/nn") && words.contains("earth/nn")) {System.out.println("file is "+filename);}    
        for(i=0;i<words.size();i++)
        {
        	//System.out.println(words[i]+" "+words[i+1]+" "+words[i+2]);
        	
        	ArrayList<String>neighbours=new ArrayList<String>();
        	ArrayList<String>senses=new ArrayList<String>();
        	//Triwords t=new Triwords(words[i].split("/")[0],words[i+1].split("/")[0],words[i+2].split("/")[0],words[i].split("/")[1],words[i+1].split("/")[1],words[i+2].split("/")[1]);
        	
        	if((i-1)>=0)
    		{
    		neighbours.add(words.get(i-1).split("/")[0]);
    		senses.add(words.get(i-1).split("/")[1]);
    		}
    	if((i-2)>=0)
    	{
    		neighbours.add(words.get(i-2).split("/")[0]);
    		senses.add(words.get(i-2).split("/")[1]);
    		}
    	if((i-3)>=0)
    	{
    		neighbours.add(words.get(i-3).split("/")[0]);
    		senses.add(words.get(i-3).split("/")[1]);
    		}
    	if((i+1)<words.size())
    	{
    		neighbours.add(words.get(i+1).split("/")[0]);
    		senses.add(words.get(i+1).split("/")[1]);
    		}
    	if((i+2)<words.size())
    	{
    		neighbours.add(words.get(i+2).split("/")[0]);
    		senses.add(words.get(i+2).split("/")[1]);
    		}
    	
    	if((i+3)<words.size())
    	{
    		neighbours.add(words.get(i+3).split("/")[0]);
    		senses.add(words.get(i+3).split("/")[1]);
    		}
        	if(trigrams.containsKey(words.get(i).split("/")[0]))
        	{
        		//System.out.println(words[i]);
        		if(wordCount.get(words.get(i).split("/")[0])>800) System.out.println("more  of "+words.get(i));
        		wordCount.put(words.get(i).split("/")[0], wordCount.get(words.get(i).split("/")[0])+1);
        		//trigrams.put(words[i], trigrams.get(words[i]) + 1);
        	Triwords t_old=	(Triwords)trigrams.get(words.get(i).split("/")[0]);
        	ArrayList<String>n=new ArrayList<String>();
        	ArrayList<String>s=new ArrayList<String>();
        	n=t_old.words;
        	s=t_old.senses;
        	Iterator<String> word = n.iterator();
        	Iterator<String> sense= s.iterator();
        	while(word.hasNext())
        	{
        	    String w = word.next();
        	    String se=sense.next();
        	    neighbours.add(w);
        	    senses.add(se);
        	    //Do something with obj
        	}
        	Triwords t = new Triwords(neighbours,senses,1);	
        		
        		trigrams.put(words.get(i).split("/")[0], t);
        	}	
        		else
        		{
            		//trigrams.put(words[i], trigrams.get(words[i]) + 1);
        		//	System.out.println(words[i].split("/")[0]);
            	Triwords t = new Triwords(neighbours,senses,1);	
        		trigrams.put(words.get(i).split("/")[0], t);
        		wordCount.put(words.get(i).split("/")[0], 1);
        		
        		}
        
        
        }
    } finally {
        br.close();
    }
	return trigrams;
	
	
	
	
}
}